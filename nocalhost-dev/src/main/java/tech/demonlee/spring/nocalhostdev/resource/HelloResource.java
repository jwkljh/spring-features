package tech.demonlee.spring.nocalhostdev.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author Demon.Lee
 * @date 2023-04-11 22:56
 * @desc api for greeting someone
 */
@RestController
@RequestMapping("/greet")
public class HelloResource {

    @GetMapping()
    public String greet() {
        return "Hello, " + UUID.randomUUID() + "\n";
    }

    @GetMapping("/{nickName}")
    public String greetWithName(@PathVariable("nickName") String nickName) {
        return "Hi, " + nickName + "\n";
    }
}
